package com.enedis.syncapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SyncapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SyncapiApplication.class, args);
	}
	
}
