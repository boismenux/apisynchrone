package com.enedis.syncapi.model;

import java.util.List;

public class AggregatedLoadCurve {
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getAreaID() {
		return areaID;
	}
	public void setAreaID(String areaID) {
		this.areaID = areaID;
	}
	public List<IntervalBlock> getIntervalBlocks() {
		return intervalBlocks;
	}
	public void setIntervalBlocks(List<IntervalBlock> intervalBlocks) {
		this.intervalBlocks = intervalBlocks;
	}
	private String unit;
	private String areaID;
	private List<IntervalBlock> intervalBlocks;
}
