package com.enedis.syncapi.model;

public class IntervalBlock {
	
	public IntervalBlock(String timestamp, String value) {
		this.timestamp = timestamp;
		this.value = value;
	}

	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private String timestamp;
	private String value;
}
