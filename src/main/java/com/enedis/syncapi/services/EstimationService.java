package com.enedis.syncapi.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.enedis.syncapi.model.AggregatedLoadCurve;
import com.enedis.syncapi.model.IntervalBlock;


@Component
public class EstimationService {

	private static AggregatedLoadCurve aggregatedProductionLoadCurve = new AggregatedLoadCurve();
	private static AggregatedLoadCurve aggregatedConsumptionLoadCurve = new AggregatedLoadCurve();
	private static List<IntervalBlock> intervalBlocks = new ArrayList<>();
	static {
		//Initialize Data
		aggregatedProductionLoadCurve.setUnit("Wh");
		aggregatedConsumptionLoadCurve.setUnit("Wh");

		IntervalBlock intervalBlocks1 = new IntervalBlock("2016-02-23T08:20:50.52Z", "10000");
		IntervalBlock intervalBlocks2 = new IntervalBlock("2016-02-23T08:30:50.52Z", "11000");

		intervalBlocks.add(intervalBlocks1);
		intervalBlocks.add(intervalBlocks2);
		aggregatedProductionLoadCurve.setIntervalBlocks(intervalBlocks);
		aggregatedConsumptionLoadCurve.setIntervalBlocks(intervalBlocks);
	}
	public AggregatedLoadCurve retrieveProductionLoadCurve(String areaId) {
		aggregatedProductionLoadCurve.setAreaID(areaId);
		return aggregatedProductionLoadCurve;
	}
	
	public AggregatedLoadCurve retrieveConsumptionLoadCurve(String areaId) {
		aggregatedConsumptionLoadCurve.setAreaID(areaId);
		return aggregatedConsumptionLoadCurve;
	}

}
