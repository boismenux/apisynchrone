package com.enedis.syncapi.controllers;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/hive")
public class HiveController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	@Qualifier("hiveJdbcTemplate")
	private JdbcTemplate hiveJdbcTemplate;
	
	@RequestMapping("/select")
	public String select() {
		String sql = "select * from productions";
		logger.debug("Hive SQL :"+sql);
		List<Map<String, Object>> rows = hiveJdbcTemplate.queryForList(sql);
		Iterator<Map<String, Object>> it = rows.iterator();
		while (it.hasNext()) {
			Map<String, Object> row = it.next();
			logger.debug("Hive result: "+String.format("%s\t%s", row.get("productions.horodatage"), row.get("productions.value")));
		}
		return "Done";
	}

}
