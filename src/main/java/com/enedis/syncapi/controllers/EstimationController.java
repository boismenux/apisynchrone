package com.enedis.syncapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.enedis.syncapi.model.AggregatedLoadCurve;
import com.enedis.syncapi.services.EstimationService;

@RestController
class EstimationController {

	@Autowired
	private EstimationService estimationService;
  
  @GetMapping("/estimated_production_load_curve/per_area/{areaId}")
	public AggregatedLoadCurve retrieveProductionLoadCurve(@PathVariable String areaId) {
		return estimationService.retrieveProductionLoadCurve(areaId);
	}
  
  @GetMapping("/estimated_consumption_load_curve/per_area/{areaId}")
 	public AggregatedLoadCurve retrieveConsumptionLoadCurve(@PathVariable String areaId) {
 		return estimationService.retrieveConsumptionLoadCurve(areaId);
 	}
}
